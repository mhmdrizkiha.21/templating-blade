@extends('cast.master')
@section('content')
<div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
    <div class="card bg-light d-flex flex-fill">
      <div class="card-header text-muted border-bottom-0">
        Cast {{$cast->id}}
      </div>
      <div class="card-body pt-0">
        <div class="row">
          <div class="col-12">
            <h2 class="lead"><b> {{$cast->nama}} </b></h2>
            <p class="text-muted text-sm"><b>Umur: </b> {{$cast->umur}} </p>
            <p class="text-muted text-sm"><b></b> {{$cast->bio}} </p>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection