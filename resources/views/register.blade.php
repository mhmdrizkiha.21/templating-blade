<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form - Page</title>
</head>
<body>
    <h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h5><br>   
    <form action= {{ route('kirim') }} method="POST"> 
        @csrf
        <label for="">First Name : </label><br><br>
        <input type="text" name="nama_depan" id=""><br><br>

        <label for="">Last Name : </label><br><br>
        <input type="text" name="nama_belakang" id=""><br><br>

        <label for="">Gender</label><br><br>
        <input type="radio" name="" id="">Male<br>
        <input type="radio" name="" id="">Female<br><br>

        <label for="">Nationallity</label><br><br>
        <select name="" id="">
            <option value="">Indonesia</option>
            <option value="">Amerika</option>
            <option value="">Inggris</option>
        </select><br><br>
        <label for="">Language Spoken</label><br><br>
        <input type="checkbox" name="" id="">Bahasa Indonesia<br>
        <input type="checkbox" name="" id="">English<br>
        <input type="checkbox" name="" id="">Other<br><br>

        <label for="">Bio</label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br>
        <button type="submit">SignUp</button>

    </form>
</body>
</html>